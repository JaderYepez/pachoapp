from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView
from ExcelImporter.forms import CustomersForm, CreateFileForm
from ExcelImporter.models import customers, file_parcer
from django.utils.dateparse import parse_date
from openpyxl import Workbook
import re


def index(request):
    return render(request, 'index.html')


class Customers(CreateView):
    model = customers
    template_name = 'create_user.html'
    form_class = CustomersForm
    success_url = reverse_lazy('index')


class CreateFile(CreateView):
    model = file_parcer
    template_name = 'create_file.html'
    form_class = CreateFileForm
    success_url = reverse_lazy('index')

    def model_form_upload(request):
        if request.method == 'POST':
            form = CreateFileForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('home')
        else:
            form = CreateFileForm()
        return render(request, 'index.html', {
            'form': form
        })


class Filter(ListView):
    model = customers, file_parcer
    template_name = 'Filter.html'
    form_class = CustomersForm

    def get_context_data(self, **kwargs):
        contex = super(Filter, self).get_context_data(**kwargs)
        contex['customer'] = customers.objects.filter(file_parcer__isnull=False).distinct()
        # almacena la consulta momentaneamente pero para pasarla a la otra clase debemos
        # en la template pasar el argumento search
        if re.search("[?].+", self.request.get_full_path()):
            contex["search"] = u""+re.search("[?].+", self.request.get_full_path()).group()
        else:
            contex["search"] = ""
        print(contex['customer'])
        return contex

    def get_queryset(self):
        cus = self.request.GET.get('cus', '')
        opt = self.request.GET.get('opt', '')
        sen = self.request.GET.get('sen', '')
        dateS = self.request.GET.get('dateS', '')
        dateF = self.request.GET.get('dateF', '')
        ds = parse_date(dateS)
        df = parse_date(dateF)

        # FILTER
        if cus and opt and sen and ds and df:
            result=file_parcer.objects.filter(
                customerId__name=cus, option=opt,sended=sen,emailDate__gte=ds, emailDate__lte=df)
            return result
        elif cus and opt and sen and ds:
            result = file_parcer.objects.filter(
                customerId__name=cus, option=opt,sended=sen,emailDate=ds)
            return result
        elif cus and opt and sen and df:
            result = file_parcer.objects.filter(
                customerId__name=cus, option=opt,sended=sen,emailDate=df)
            return result
        elif cus and opt and sen:
            result = file_parcer.objects.filter(
                customerId__name=cus, option=opt,sended=sen)
            return result
        elif cus and opt:
            result = file_parcer.objects.filter(
                customerId__name=cus, option=opt)
            return result
        elif cus:
            result = file_parcer.objects.filter(
                customerId__name=cus)
            return result
        elif ds and df:
            result = file_parcer.objects.filter(
                emailDate__gte=ds, emailDate__lte=df)
            return result
        elif ds:
            result = file_parcer.objects.filter(
                emailDate=ds)
            return result
        elif df:
            result = file_parcer.objects.filter(
                emailDate=df)
            return result
        elif cus or opt or sen:
            result = file_parcer.objects.filter(
                Q(customerId__name=cus) | Q(option=opt) | Q(sended=sen)
            )
            return result
        result=file_parcer.objects.all()
        return result


class ReportExcel(TemplateView):
    def get(self, request, *args, **kwargs):
        # Esta Linea rescata la consulta envia desde el search la cual puedo utilizar
        # para el reporte
        reports = Filter(**{'request':request}).get_queryset()
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'Report Dates Excel'

        ws.merge_cells('B1:E1')

        ws['B3'] = 'User'
        ws['C3'] = 'File'
        ws['D3'] = 'Type Created'
        ws['E3'] = 'Sended'
        ws['F3'] = 'Date'

        count = 5

        for report in reports:
            ws.cell(row=count, column=2).value = str(report.customerId)
            ws.cell(row=count, column=3).value = str(report.file)
            ws.cell(row=count, column=4).value = str(report.option)
            ws.cell(row=count, column=5).value = str(report.sended)
            ws.cell(row=count, column=6).value = str(report.emailDate)
            count += 1

        file_name = "ReporterFilter.xlsx"
        response = HttpResponse(content_type='application/ms-excel')
        content = "attachment; filename = {0}".format(file_name)
        response['Content-Disposition'] = content
        wb.save(response)
        return response
