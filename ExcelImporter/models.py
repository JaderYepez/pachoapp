from django.db import models

PAR = (
    ('A', 'Primera'),
    ('B', 'Segunda'),
    ('C', 'Tercera'),
)


class customers(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    parser = models.CharField(max_length=1, choices=PAR)

    def __str__(self):
        return self.name


class file_parcer(models.Model):
    OPT = (
        ('byEmail', 'Create_Email'),
        ('uploadByUser','Create_User')
    )
    idFileParser = models.AutoField(primary_key=True)
    customerId = models.ForeignKey(customers, on_delete=models.CASCADE)
    emailId = models.IntegerField(blank=True)
    emailDate = models.DateField(blank=True)
    parser = models.CharField(max_length=1, choices=PAR)
    option = models.CharField(max_length=20, choices=OPT, blank=True)
    sended = models.BooleanField(default=False)
    file = models.FileField(upload_to='images/')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.customerId)+" - "+self.option
