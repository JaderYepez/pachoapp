from django import forms


class FormularioContacto(forms.Form):
    correo= forms.EmailField()
    mensaje= forms.CharField()


class Formulario(forms.Form):
    #start_date_time = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'date'}))
    nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    mail = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control', 'type': 'text'}))
    mensaje = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control col-lg-12'}))


class FormularioAdjunto(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    mail = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control', 'type': 'text'}))
    mensaje = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control col-log-12'}))
    archivo = forms.FileField()


class FormularioDestinos(forms.Form):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    mensaje = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control col-log-12'}))
    archivo = forms.FileField()
    contactos = forms.FileField()


# class ContacForm(forms.Form):
#     name = forms.CharField(label='Nombre', widget=forms.TextInput)
#     email = forms.EmailField(label='Correo Electrinico', widget=forms.EmailInput)
#     message = forms.CharField(label='Mensaje', widget=forms.Textarea,)
