from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from ExcelImporter.models import customers, file_parcer
from import_export import resources


@admin.register(customers, file_parcer)
class ViewAdmin(ImportExportModelAdmin):
    pass


class File_Parcer_Export(resources.ModelResource):

    class Meta:
        model = file_parcer




#admin.site.register(customers)
#admin.site.register(file_parcer)
