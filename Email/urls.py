from django.urls import path

from Email import views
#from Email.views import HomeView #,contact
from Email.views import contacto, gracias


urlpatterns = [
    #path('', HomeView.as_view(), name='index'),
    #path('', views.contact , name='index'),
    path('contacto/', views.contacto, name='contacto'),
    path('gracias/', views.gracias, name='gracias'),
    path('adjuntarmail/', views.adjuntarmail, name='adjuntarmail'),
    path('multiplemail/', views.multiplemail, name='multiplemail'),
]