from django.urls import path
from ExcelImporter.views import Customers, CreateFile, Filter, ReportExcel
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('create_user', Customers.as_view(), name='create'),
    path('create_file', CreateFile.as_view(), name='create_file'),
    path('filter', Filter.as_view(), name='filter'),
    path('reportexcel/', ReportExcel.as_view(), name='reportexcel'),
    #path('getdates',views.GetDates,name='getdates')
]