from django.apps import AppConfig


class ExcelimporterConfig(AppConfig):
    name = 'ExcelImporter'
