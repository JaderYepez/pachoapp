import os
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.core.files.storage import default_storage
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
import smtplib, ssl, csv
from django.shortcuts import  render
from AppPacho.local_settings import PASSWORD
from Email.forms import Formulario, FormularioAdjunto, FormularioDestinos


def contacto(request):
    if request.method == 'POST':
        form = Formulario(request.POST)
        if form.is_valid():
            #email = request.POST.get('mail')
            port = 465   # For SSL
            smtp_server = "smtp.gmail.com"
            sender_email = "testdesarrollojader@gmail.com"  # Enter your address
            receiver_email = request.POST.get("mail")  # Enter receiver address
            password =  PASSWORD
            message = request.POST.get("mensaje")

            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(sender_email, receiver_email, message)

            return HttpResponseRedirect('/Email/gracias/')
    else:
        form = Formulario()

    return render(request, 'home.html', {'form': form})


def adjuntarmail(request):
    if request.method == 'POST' and request.FILES['archivo']: # Pide el Archivo adjunto
        form = FormularioAdjunto(request.POST, request.FILES)
        if form.is_valid():
            subject = request.POST.get("nombre")
            body = request.POST.get("mensaje")
            sender_email = "testdesarrollojader@gmail.com"  # Enter your address
            receiver_email = request.POST.get("mail")  # Enter receiver address
            file = request.FILES['archivo'] # Recupera el Archivo Adjunto
            password = PASSWORD
            #message = request.POST.get("mensaje")

            # Guardando la Imagen
            save_path = os.path.join(settings.MEDIA_ROOT, 'uploads', file.name)
            filename = default_storage.save(save_path, file)

            # Create a multipart message and set headers
            message = MIMEMultipart()
            message["From"] = sender_email
            message["To"] = receiver_email
            message["Subject"] = subject
            message["Bcc"] = receiver_email  # Recommended for mass emails

            # Add body to email
            message.attach(MIMEText(body, "plain"))

            # Open PDF file in binary mode
            with open(filename, "rb") as attachment:
                # Add file as application/octet-stream
                # Email client can usually download this automatically as attachment
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.read())

            # Encode file in ASCII characters to send by email
            encoders.encode_base64(part)

            # Add header as key/value pair to attachment part
            part.add_header(
                "Content-Disposition",
                f"attachment; filename= {filename}",
            )

            # Add attachment to message and convert message to string
            message.attach(part)
            text = message.as_string()

            # Log in to server using secure context and send email
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(sender_email, receiver_email, text)

            return HttpResponseRedirect('/Email/gracias/')
    else:
        form = FormularioAdjunto()

    return render(request, 'correo_adjunto.html', {'form': form})


def multiplemail(request):
    if request.method == 'POST' and request.FILES['archivo'] and request.FILES['contactos'] :
        form = FormularioDestinos(request.POST, request.FILES)
        if form.is_valid():
            subject = request.POST.get("nombre")
            body = request.POST.get("mensaje")
            sender_email = "testdesarrollojader@gmail.com"  # Enter your address
            emails = request.FILES['contactos']  # Archivo que contiene los contactos
            file = request.FILES['archivo'] # Recupera el Archivo Adjunto
            password = PASSWORD

            # Guardando la Imagen
            save_path = os.path.join(settings.MEDIA_ROOT, 'uploads', file.name)
            filename = default_storage.save(save_path, file)

            # guarda contactos
            save_path = os.path.join(settings.MEDIA_ROOT, 'uploads', emails.name)
            fileemails = default_storage.save(save_path, emails)

            # Adjuntando Correo Multiple
            with open(fileemails) as email:
                reader = csv.reader(email)
                next(reader)
                for name, email, grade in reader:
                    # Create a multipart message and set headers
                    message = MIMEMultipart()
                    message["From"] = sender_email
                    message["To"] = email
                    message["Subject"] = subject
                    message["Bcc"] = email  # Recommended for mass emails

                    #Add body to email
                    message.attach(MIMEText(body, "plain"))

                    # # Open PDF file in binary mode
                    with open(filename, "rb") as attachment:
                        # Add file as application/octet-stream
                        # Email client can usually download this automatically as attachment
                        part = MIMEBase("application", "octet-stream")
                        part.set_payload(attachment.read())

                    # Encode file in ASCII characters to send by email
                    encoders.encode_base64(part)

                    # Add header as key/value pair to attachment part
                    part.add_header(
                         "Content-Disposition",
                         f"attachment; filename= {filename}",
                    )

                    # Add attachment to message and convert message to string
                    message.attach(part)
                    text = message.as_string()

                    # Log in to server using secure context and send email
                    context = ssl.create_default_context()
                    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
                         server.login(sender_email, password)
                         server.sendmail(sender_email, email, text)

            return HttpResponseRedirect('/Email/gracias/')
    else:
        form = FormularioDestinos()

    return render(request, 'correo_multiple.html', {'form': form})


def gracias(request):
    html = '<html>' \
                '<body>"Gracias Por enviarnos su comentario ......"' \
                    '<a href="http://127.0.0.1:8000/ImporExcel/">Ir a Index</a>'\
                '</body>' \
           '</html>'
    return HttpResponse(html)