# Generated by Django 2.2 on 2019-07-19 13:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ExcelImporter', '0006_auto_20190717_1919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file_parcer',
            name='option',
            field=models.CharField(choices=[('byEmail', 'Create_Email'), ('uploadByUser', 'Create_User')], max_length=20),
        ),
    ]
