from django import forms
from ExcelImporter.models import customers, file_parcer


class CreateFileForm(forms.ModelForm):

    class Meta:

        model = file_parcer

        fields = [
            'customerId',
            'emailId',
            'emailDate',
            'parser',
            'option',
            'sended',
            'file',
        ]
        labels = {
            'customerId':'Customers',
            'emailId':'Email Id',
            'emailDate':'Email Data',
            'parser':'Parser',
            'option':'Option',
            'sended':'Sended',
            'file':'File',
        }
        widgets = {
            'customerId':forms.Select(attrs={'class':'form-control'}),
            'emailId':forms.NumberInput(attrs={'class':'form-control'}),
            'emailDate':forms.DateTimeInput(attrs={'class':'form-control'}),
            'parser':forms.Select(attrs={'class':'form-control'}),
            'option':forms.Select(attrs={'class':'form-control'}),
            'sended':forms.CheckboxInput(),
            'file': forms.FileInput(attrs={'class':'custom-file'}),
        }


class CustomersForm(forms.ModelForm):

    class Meta:

        model = customers

        fields = [
            'name',
            'last_name',
            'parser',
        ]
        labels = {
            'name':'Name',
            'last_name':'Last_name',
            'parser':'Parser',
        }
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control'}),
            'last_name':forms.TextInput(attrs={'class':'form-control'}),
            'parser':forms.Select(attrs={'class':'form-control'}),
        }